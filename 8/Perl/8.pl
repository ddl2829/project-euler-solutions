#!/usr/bin/perl -w
use strict;

#For simplicity's sake, I have extracted the number used for this problem into one long line on a text file
#named number.txt in this folder

#Read the contents of number.txt
my $number = `cat ../number.txt`;
#Temp variable holding the greatest product found
my $greatestProduct = 0;
#We know the number is 1000 digits, so we index up to 995 (-1 for index 0)
for(my $index = 0; $index < 996; ++$index) {
	#Multiply the number at this index and the next 4 together
	my $product = substr($number, $index, 1) * substr($number, $index+1, 1) * substr($number, $index+2, 1) * substr($number, $index+3, 1) * substr($number, $index+4, 1);
	#If this product is greater than the previous highest, set the new highest
	if($product > $greatestProduct) {
		$greatestProduct = $product;
		print "Greatest Product: $greatestProduct\n";
	}	
}
print "Greatest Product of 5 consecutive digits in the 1000 digit number: $greatestProduct\n";
