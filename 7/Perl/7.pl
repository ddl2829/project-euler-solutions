#!/usr/bin/perl -w
use strict;

#List the first several primes for no real reason
my @primes = (2, 3, 5, 7, 11);

#Loop over all numbers, until we have 1001 values in the primes array
for(my $number = 13; !exists($primes[1000]); ++$number) {
	#Loop over divisors for this number
	for(my $divisor = 2; $divisor < $number / 2; ++$divisor) {
		#If this number is evenly divisible by the divisor, it is not prime
		if($number % $divisor == 0) {
			last;
		}
		#If we made it to the midpoint of the number then it is prime, we exhausted all divisors
		if($divisor >= ($number / 2) - 1) {
			#Push it to the primes array
			push(@primes, $number);
		}
	}
}
print "The 1001st prime number is ", $primes[1000], "\n";
