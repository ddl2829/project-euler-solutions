#!/usr/bin/perl -w
use strict;

#Initialize an array of numbers, this will holdthe palindromes found
my @numbers;
#Loop over all numbers 100-500 incrementing by 1
for(my $number1 = 100; $number1 < 500; ++$number1) {
	#Loop over all numbers 999-500 decrementing by 1
	for(my $number2 = 999; $number2 >= 500; --$number2) {
		#The reversed loops eliminate collisions, multiply the two numbers
		my $mul = $number1 * $number2;
		#if the number is the same forwards and backwards (is a palindrome)
		if($mul eq reverse($mul)){
			#push it to the back of the array
			push(@numbers, $mul);
		}
	}
}
#Sort the list of palindromes by value
my @palindromes = sort { $a <=> $b } @numbers;
#Print the palindrome at the end of the list (the largest)
print "The largest number palindrome of 3 digit factors is ", $palindromes[scalar(@numbers)-1], "\n";
