#!/usr/bin/perl -w
use strict;

#Initialize a hash of factors, and the number we're factoring
my %factors;
my $number = 600851475143;
my $OrigNumber = $number;

#Start our divisor at 2, work up to half the number
for(my $divisor = 2; $divisor < $number / 2; ++$divisor) {
	#If the number is evenly divisible by the divisor
	if($number % $divisor == 0) {
		#If the divisor is not already recorded in the factors hash, create it
		if(!exists($factors{$divisor})) {
			$factors{$divisor} = 1;
		}
		#Divide the number at the divisor, reset the divisor to 2
		$number /= $divisor;
		$divisor = 2;
		#This will consitently divide the number by the smallest factor possible until a full
		#factorization is attained
	}
}

#Initialize  a variable to hold the largest prime found
my $largestPrime = 0;
#Loop over the hash
while((my $key, my $value) = each %factors) {
	#Set a prime flag
	my $Prime = 1;
	#Loop to find a divisor
	for(my $divisor = 2; $divisor < $key / 2; ++$divisor) {
		#If the key divides evenly, it is not prime, set the flag to 0 and exit the loop
		if($key % $divisor == 0) {
			$Prime = 0;
			print "$divisor is not prime\n";
			last;
		}
	}
	#If the number is prime
	if($Prime == 1) {
		#If the number is bigger than the previous largest prime, set it as the biggest prime
		#--Is this necessary? If the hash is looped over in numerical order by key, then the key being observed must be greater
		if($key > $largestPrime) {
			$largestPrime = $key;
		}
	}
}

print "Largest prime factor of $OrigNumber: $largestPrime\n";
