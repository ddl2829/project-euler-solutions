#!/usr/bin/perl -w
use strict;

#Initialize 2 variables to hold the sum of squares and the square of sums
my $sumOfSquares = 0;
my $squareOfSums = 0;

#Loop over all numbers 1-100
for(my $num = 1; $num < 101; ++$num) {
	#Add the number to the square of sums
	$squareOfSums += $num;
	#Square the number and add the result to sum of squares
	$sumOfSquares += ($num * $num);
}
#Square the square of sums
$squareOfSums *= $squareOfSums;
print "Sum of Squares: $sumOfSquares\n";
print "Square of Sums: $squareOfSums\n";

#Subtract the values to get the difference, use absolute values for large values that may cross over
my $difference = abs($squareOfSums - $sumOfSquares);
print "Difference: $difference\n";
