package MultiplesOf3And5;

import java.util.ArrayList;
import java.util.List;

/**
 * If we list all the natural numbers below 10 that 
 * are multiples of 3 or 5, we get 3, 5, 6 and 9. 
 * The sum of these multiples is 23.
 * Find the sum of all the multiples of 3 or 5 below 1000.
 *
 * @author dale
 */
public class java {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int limit = 1000;
        System.out.println(String.format("The multiples of 3 and 5 below %s are: ", String.valueOf(limit)));
        
        int sum = 0;
        for(int i = 1; i < limit; ++i) {
            if(isMultipleOf(3, i) || isMultipleOf(5, i)) {
                sum += i;
                System.out.println(String.valueOf(i));
            }
        }
        
        System.out.println(String.format("The sum of these multiples is %s", String.valueOf(sum)));
    }
    
    public static boolean isMultipleOf(int multipleOf, int check) {
        return check % multipleOf == 0;
    }
    
}
