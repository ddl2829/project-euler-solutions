#!/usr/bin/perl -w
use strict;

my $sum = 0;
#For each number below 1000
for(my $int = 0; $int < 1001; ++$int) {
	#If the number is evenly divisible by 3 or 5, add it to the sum
	if($int % 3 == 0 || $int % 5 == 0) {
		$sum += $int;
	}
}
print "Sum of all multiples of 3 or 5 below 1000: ", $sum, "\n";
