#!/usr/bin/perl -w
use strict;

#Set the first two terms of our fibonacci sequence
my @terms = ( 1, 2 );
#Initialize the sum at zero
my $totalSum = 0;
#Generate numbers until we reach a term exceeding 4 million
for(my $index = 1; $terms[$index] < 4000000; ++$index) {
	#Add the current index and the one before it to get the next fibonacci number
	my $sum = $terms[$index] + $terms[$index-1];
	#If the number is evenly divisible by 2, it is even so add it to the sum
	if($sum % 2 == 0) {
		$totalSum += $sum;
	}
	push(@terms, $sum);
}
print "Sum of the even fibonacci numbers starting with 1 and 2 not exceeding 4 million: ", $totalSum, "\n";
