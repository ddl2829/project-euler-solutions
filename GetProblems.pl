#!/usr/bin/perl -w
use strict;

my @range = split("-", $ARGV[0]);
for(my $index = $range[0]; $index <= $range[1]; ++$index){ 
	`mkdir $index`;
	`wget projecteuler.net/problem=$index`;
	`mv problem=$index $index/problem$index`;
	print "Fetched Problem $index\n";
}
print "Done\n";
