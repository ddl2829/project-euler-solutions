#!/usr/bin/perl -w
use strict;

#Initialize a variable to loop against, and to store the eventual value we want
my $found = 0;

#Remember the lowest value reached for feedback purposes, this is a slowish script
my $highestReached = 21;
#Loop over numbers starting at 20, and incrementing by 20. As our number must be divisible by 20,
#it must also be a multiple of 20. This makes the loop 20 times faster than incrementing by 1
for(my $number = 20; $found == 0; $number += 20) {
	#Loop over all divisors starting at 20, so we can rule out larger divisors faster for a quicker search
	for(my $divisor = 20; $divisor > 0; --$divisor) {
		#If the number divides evenly, print some debug if it's a new low divisor
		if($number % $divisor == 0) {
			if($divisor < $highestReached) {
				$highestReached = $divisor;
				print "Low divisor: $number / $divisor\n";
			}
			#If the divisor is 1, we found the number. Set it to found and exit the loops
			if($divisor == 1) {
				$found = $number;
				last;
			}
		} else {
			#If the number was not evenly divisible, exit this loop
			last;
		}	
	}
}
print "The smallest number divisible by all numbers between 1 and 20 is $found\n";
